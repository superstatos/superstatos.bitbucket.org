$(document).ready(function() {
    $('.classes-animated-1').addClass('animated pulse');
    $('.classes-animated-2').addClass('animated pulse');
    $('#ball').addClass('animated bounce');
    $('#ball-shadow').addClass('psevdo-anim');
    $('.adv1').addClass('animated');
    $('.adv2').addClass('animated');
    $('.adv3').addClass('animated');
    $('.arena1').addClass('animated');
    $('.arena2').addClass('animated');
    $('.thumbnail').addClass('animated');
    $('.adv-text').addClass('animated');

    function repeatAnimation(id, animation, timeout) {
        setInterval(function () {
            $(id).removeClass(animation);

            setTimeout(function () {
                $(id).addClass(animation);
            },300);
        },timeout);
    }

    repeatAnimation('.classes-animated-1', 'pulse', 3000);
    repeatAnimation('.classes-animated-2', 'pulse', 3500);
    repeatAnimation('#ball', 'bounce', 5000);
    repeatAnimation('#ball-shadow', 'psevdo-anim', 5000);

    $('.adv2').waypoint(function () {
        $('.adv3').addClass('slideInLeft');
        $('.adv2').addClass('slideInUp');
        $('.adv1').addClass('slideInRight');
        $('#podxod').addClass('slideInUp');
    }, { offset: '100%'});

    $('.adv-text').waypoint(function () {
        $('.adv-text').addClass('slideInUp');
    }, { offset: '100%'});

    $('.arena').waypoint(function () {
        $('.arena1').addClass('slideInLeft');
        $('.arena2').addClass('slideInRight');
    }, { offset: '100%'});

    $('.thumbnail').waypoint(function () {
        $('.thumbnail').addClass('zoomIn');
    }, { offset: '100%'});
});